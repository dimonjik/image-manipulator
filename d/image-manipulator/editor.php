<?php
	require('../settings.php');
	$IMG_DIR = $site_doc_root;
	$IMG_URL = '';

	//Vars for local debugging
	#$IMG_DIR = '/home/neox/other_projects/jq-image-manipulator/admin2/d/image-manipulator/images/';
	#$IMG_URL='/admin2/d/image-manipulator/images/';
	
	//Editor mode
	if (isset($_GET['edit']) && isset($_GET['image']) && $_GET['image']!=''){
		$image = $_GET['image'];
		if (!file_exists($IMG_DIR.$image)) die('ERROR, wrong input file');	
	}
	
	//Convertation mode
	if (isset($_GET['convert']) && isset($_POST['image']) && $_POST['image']!=""){
		$image_width = intval($_POST['image_width']);
		$image_height = intval($_POST['image_height']);
		$crop_x = intval($_POST['crop_x']);
		$crop_y = intval($_POST['crop_y']);
		$crop_width = intval($_POST['crop_width']);
		$crop_height = intval($_POST['crop_height']);
		$rotate_angle = intval($_POST['rotate_angle']);		
		
		$result = process_image($_POST['image'], $image_width, $image_height, $crop_x, $crop_y, $crop_width, $crop_height, $rotate_angle);
		require 'result.php';
		die();
	}
	
	
	function process_image($image, $image_width, $image_height, $crop_x, $crop_y, $crop_width, $crop_height, $rotate_angle){
		if (!$image || strlen($image)<2) return array(false, 'wrong input file specified');
		
		global $IMG_DIR;
		$image_url = $image; #Save url for showing in the <img/> tag
		$image = $IMG_DIR.ltrim($image, '/'); #Now store full path to image file in $image
		$result_image = $image;
		
		if (!file_exists($image)) return array(false,'input file doesnt exist');
				
		$command = '/usr/local/bin/convert';
		$command .= " {$image}";
		$command .= " -resize {$image_width}x{$image_height}";
		
		if ($rotate_angle != 0)
			$command .= " -rotate {$rotate_angle}";
		
		$command .= " -crop {$crop_width}x{$crop_height}+{$crop_x}+{$crop_y}";
		$command .= " +repage";	
		$command .= " {$result_image}";
		
		$output = `$command`;
		
		sleep(2); #Guess imagemagick might work not as fast as page loads
		
		if (file_exists($result_image)) 
			return array(true, $image_url);
		else
			return array(false, 'cant write to file');
	}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>Modal image editor</title>
		<meta http-equiv="Content-Type" content="text/html; charset=windows-1251" />
		
		<script type="text/javascript" src="/admin2/js/1.9.1/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="/admin2/js/1.9.1/jquery-ui.min.js"></script>
		<script type="text/javascript" src="/admin2/js/jQueryRotateCompressed.2.2.js"></script>
		<script type="text/javascript" src="/admin2/d/image-manipulator/jquery.jrac.js"></script>		

		<link rel="stylesheet" type="text/css" media="all" href="/admin2/js/1.9.1/jquery-ui.css" />
		<link rel="stylesheet" type="text/css" media="all" href="/admin2/d/image-manipulator/style.jrac.css" />		
		
		<script type="text/javascript">
			<!--//--><![CDATA[//><!--
			$(document).ready(function() {
				// Apply jrac on some image.
				$('.pane img').jrac({
					'crop_width' : 150,
					'crop_height' : 150,
					'crop_x' : 0,
					'crop_y' : 0,
					'viewport_onload' : function() {
						var $viewport = this;
						var inputs = $viewport.$container.parent('.pane').find('.coords input:text');
						var events = ['jrac_crop_x', 'jrac_crop_y', 'jrac_crop_width', 'jrac_crop_height', 'jrac_image_width', 'jrac_image_height', 'jrac_image_rotate'];						
						
						//Manual elements bindings to jrac events
						$viewport.observator.register('jrac_crop_x', $('input#crop_x'), function(event_name, element, value) {element.val(value)});
						$viewport.observator.register('jrac_crop_y', $('input#crop_y'), function(event_name, element, value) {element.val(value)});
						$viewport.observator.register('jrac_crop_width', $('input#crop_width'), function(event_name, element, value) {element.val(value)});
						$viewport.observator.register('jrac_crop_height', $('input#crop_height'), function(event_name, element, value) {element.val(value)});
						$viewport.observator.register('jrac_image_width', $('input#image_width'), function(event_name, element, value) {element.val(value)});
						$viewport.observator.register('jrac_image_height', $('input#image_height'), function(event_name, element, value) {element.val(value)});
						$viewport.observator.register('jrac_image_rotate', $('input#rotate_angle'), function(event_name, element, value) {element.val(value)});
						
						//Bind input fields changes to change jrac image properties
						$('input#image_width').change(function(){
							$viewport.observator.set_property('jrac_image_width', $(this).val());	
						})
						$('input#image_height').change(function(){
							$viewport.observator.set_property('jrac_image_height', $(this).val());	
						})
						
						$('input#crop_width').change(function(){
							$viewport.observator.set_property('jrac_crop_width', $(this).val());	
						})
						$('input#crop_height').change(function(){
							$viewport.observator.set_property('jrac_crop_height', $(this).val());	
						})
						
						$viewport.$container.append('<div>�������� ������ �������: ' + $viewport.$image.originalWidth + ' x ' + $viewport.$image.originalHeight + '</div>')
					}
				})
				// React on all viewport events.
				.bind('jrac_events', function(event, $viewport) {
					var inputs = $(this).parents('.pane').find('.coords input');
					inputs.css('background-color', ($viewport.observator.crop_consistent()) ? 'chartreuse' : 'salmon');
					//Disable submit button until crop is valid
					if ($viewport.observator.crop_consistent() == true)
						$('#editorSubmit').removeAttr("disabled");						
					else
						$('#editorSubmit').attr("disabled", "disabled");
				});

			});
			//--><!]]>
		</script>
	</head>
	<body>
		<div class="pane clearfix">
			<img src="<?php echo "{$IMG_URL}{$image}?refresh=".time(); ?>" alt="" />
			<form method="POST" action="/admin2/d/image-manipulator/editor.php?convert" >
				<div id="#crop_props" style="display:none">
					Cx<input name="crop_x" id="crop_x" type="text" />
					Cy<input name="crop_y" id="crop_y" type="text" />
					Ra<input name="rotate_angle" id="rotate_angle" type="text" />
					<input type="hidden" name="image" value="<?php echo "{$image}"; ?>" />
				</div>
				
				<table class="coords">
					<tr>
						<td>������ ��������</td><td><input name="image_width" id="image_width" type="text" /></td>
						<td>������ ������� ������</td><td><input name="crop_width" id="crop_width" type="text" /></td>
					</tr>
					<tr>
						<td>������ ��������</td><td><input name="image_height" id="image_height" type="text" /></td>
						<td>������ ������� ������</td><td><input name="crop_height" id="crop_height" type="text" /></td>
					</tr>
				</table>
				<input id="editorSubmit" type="submit" />
			</form>
 		</div>
	</body>
</html>