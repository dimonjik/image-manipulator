/*
 * Image preview script 
 * powered by jQuery (http://www.jquery.com)
 * 
 * written by Alen Grakalic (http://cssglobe.com)
 * 
 * for more info visit http://cssglobe.com/post/1695/easiest-tooltip-and-image-preview-using-jquery
 *
 */
 
this.imagePreview = function(){	
	/* CONFIG */
		
		xOffset = 10;
		yOffset = 30;
		
		// these 2 variable determine popup's distance from the cursor
		// you might want to adjust to get the right result
		
	/* END CONFIG */
	$("span.filename-pic").hover(function(e){
		this.t = this.title;
		this.title = "";	
//		var c = (this.t != "") ? "<br/>" + this.t : "";
		var c ="";
		$("body").append("<p id='filename-pic'><img src='"+ this.t + "?" +  new Date().getTime() +"' alt='Image filename-picture' />"+ c +"</p>");								 
		$("#filename-pic")
			.css("top",(e.pageY - xOffset) + "px")
			.css("left",(e.pageX + yOffset) + "px")
			.fadeIn("fast");						
    },
	function(){
		this.title = this.t;	
		$("#filename-pic").remove();
    });	
	$("span.filename-pic").mousemove(function(e){
		$("#filename-pic")
			.css("top",(e.pageY - xOffset) + "px")
			.css("left",(e.pageX + yOffset) + "px");
	});			
};


// starting the script on page load
$(document).ready(function(){
	imagePreview();
});